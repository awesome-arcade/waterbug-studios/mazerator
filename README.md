# Mazerator

![GitHub repo size](https://img.shields.io/github/repo-size/Awesome-Arcade/Mazerator)
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/Awesome-Arcade/Mazerator)
![Lines of code](https://img.shields.io/tokei/lines/github/MazeratorGame/Mazerator)
![GitHub Workflow Status](https://img.shields.io/github/workflow/status/Awesome-Arcade/Mazerator/dev)
![CircleCI](https://img.shields.io/circleci/build/github/MazeratorGame/Mazerator)
![GitHub language count](https://img.shields.io/github/languages/count/Awesome-Arcade/Mazerator)
![GitHub top language](https://img.shields.io/github/languages/top/Awesome-Arcade/Mazerator)
![GitHub all releases](https://img.shields.io/github/downloads/Awesome-Arcade/Mazerator/total)
![GitHub issues](https://img.shields.io/github/issues/Awesome-Arcade/Mazerator)
![GitHub pull requests](https://img.shields.io/github/issues-pr/Awesome-Arcade/Mazerator)
![GitHub](https://img.shields.io/github/license/Awesome-Arcade/Mazerator)
![GitHub contributors](https://img.shields.io/github/contributors/Awesome-Arcade/Mazerator)
![GitHub commit activity](https://img.shields.io/github/commit-activity/m/MazeratorGame/Mazerator)
![GitHub last commit](https://img.shields.io/github/last-commit/Awesome-Arcade/Mazerator)
[![gitlocalized ](https://gitlocalize.com/repo/5634/whole_project/badge.svg)](https://gitlocalize.com/repo/5634/whole_project?utm_source=badge)
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)


Mazerator game for Weekly Game Jam Week #126, but failed to submit on time https://awesomekalin.itch.io/mazerator
If you want to install old versions of Mazerator, goto github.com/Awesome-Arcade/Mazerator-old/releases

# Why I switch to Godot
One reason: Quicker (There are actually more)

# How to build
Open Godot 3.3 (must be this!) then open this project and then goto Project > Export and if you don't have the export templates, download them and then select your platform or what platform you want to build to. Then press Export Project. It may ask you where to export. If you do not do the correct version of godot, I/we will NOT provide support

# Releases page
Please note that releases on the release page aren't full releases! Full releases will have f at the end!!!

# Donate!
To donate either use the funding/sponsors section or go to [https://commerce.coinbase.com/checkout/766db0f0-8969-47b0-ab97-fbc6beda44dd](https://commerce.coinbase.com/checkout/766db0f0-8969-47b0-ab97-fbc6beda44dd)

You can also directly donate to me at the following crypto addresses:

Bitcoin: bc1q322ch40lqj33fv4p2k9v65mah6wukhz8yjmmfw
Ethereum: 0x500df13CF0EB8aF05035CC3DdA0e64130DA2b755

Cardano: addr1q87tr6z3j5avj8k45wm8z609n3tsx3mlqjnk8arn705ejehuk859r9f6ey0dtgakw957t8zhqdrh7p98v0688ulfn9nqgqn5vq

Binance Coin on Binance Chain Network: bnb1657p6svdez6s62g6hrn903s9ju6hqvph753n75

Tether on Ethereum: 0x500df13CF0EB8aF05035CC3DdA0e64130DA2b755

XRP: rKy2uy2fDVBqEHcBS2zuramPdqaL7Xk54x

USD Coin on Ethereum: 0x500df13CF0EB8aF05035CC3DdA0e64130DA2b755

Polkadot: 16773Bb9dgC2C8rueXyZqmUqSo3thHEfBBQQFnMGioSMSANU

Uniswap: 0x500df13CF0EB8aF05035CC3DdA0e64130DA2b755

Chainlink: 0x500df13CF0EB8aF05035CC3DdA0e64130DA2b755

Litecoin: LNnEqbG5RHYJEWZB8fCJG7hDHVcR5cn3YC

Bitcoin Cash: qzejd8pcfzz435mkt78jhvra4nqus8d44qvlg0er39

Cosmos: cosmos1rmctpc9qvsjvc9fr3w5u98dm3tclhnppjsyzh2

Stellar: GAMFDMLLD7L5IX7ZZKOY2NYS37XOEZS4NWWKZFIDI57UXFFGAJ3FNK6G

VeChain: 0x7F4da80687505bd9f2CE903A5a0a9134c3e49eB0

Tron: TRKTBWqapzqRWGfww2WVWGC3R6G3GCjo3L

Dai: 0x500df13CF0EB8aF05035CC3DdA0e64130DA2b755

Tezos: tz1W8LDfib6FZ2V9tU31Jq9NArEVANkTArhz

NEO: AcZHapSv9bCYiQ6dk8n9WMxhsv8LwhU8Z6

NEM: NAPBLJT7XSC7GWHS2VOXLOTCZV7UJVHKRRR5RFCV

Dogecoin: DTb5ptM4Ek32ZWfT98RdcarUNz1ezQQnpj

Monero: 43vvVaF4FqHedNC5Y5RxVoTFhvKSC842Yd4nhDbSVtT8PqWdXhhyqzidJu6m3qT12h9juGVQkSzuNQusueHot1hB7uS2TR1
